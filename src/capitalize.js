module.exports = function capitalize(a) {
    if (typeof a !== 'string') {
        throw new Error('bad input');
    }
    
    return a[0].toUpperCase() + a.slice(1);
};
  
