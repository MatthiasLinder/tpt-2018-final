const config = require('../../nightwatch.conf.js');

module.exports = {
    '@tags': ['Google'],
    'Google search for \'Tallinn\'': function (browser) {
    // load google
        browser
            .resizeWindow(1920, 1080)
            .url('http://Google.com/')
            .waitForElementVisible('body')
            .setValue('input[class="gLFyf gsfi"]', ['tallinn', browser.Keys.ENTER])
            .waitForElementVisible('body')
            .pause(500)
            .assert.containsText('body', 'tallinn')
            .saveScreenshot(`${config.imgpath(browser)}tallinnGoogleSearch.png`)
            .click('a[href="https://en.wikipedia.org/wiki/Tallinn"]')
            .waitForElementVisible('body')
            .saveScreenshot(`${config.imgpath(browser)}firstGoogleResult.png`)
            .end();
    },
};
