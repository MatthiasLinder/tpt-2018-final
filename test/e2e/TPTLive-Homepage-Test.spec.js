const config = require('../../nightwatch.conf.js');

module.exports = {
    '@tags': ['Tptlive'],
    'Go to class timetable and take screenshot': function (browser) {
    // load google
        browser
            .resizeWindow(1920, 1080)
            .url('https://www.tptlive.ee/')
            .waitForElementVisible('body')
            .saveScreenshot(`${config.imgpath(browser)}tptliveHomepage.png`)
            .click('li[id="menu-item-1313"]')
            .waitForElementVisible('body')
            //.assert.containsText('body', 'tallinn')
            .click('a[href="https://tpt.siseveeb.ee/veebivormid/tunniplaan/tunniplaan?oppegrupp=226&nadal=03.12.2018"]')
            .waitForElementVisible('body')
            .saveScreenshot(`${config.imgpath(browser)}ta17eTimetable.png`)
            .pause(1000)
            .end();
    },
};
